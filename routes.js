exports.f = function(fs, express, app, getip, request, dossier) {
	
	app.get(/static\/([a-z0-9\.\/_-]+)$/i, function(req, res) {
		res.sendFile(dossier + '/static/' + req.params[0]);
	});

	app.get(/fonts\/([a-z0-9\.\/_-]+)$/i, function(req, res) {
		res.sendFile(dossier + '/static/fonts/' + req.params[0]);
	});

	app.get(/media\/([a-z0-9\.\/_-]+)$/i, function(req, res) {
		res.sendFile('/media/pi/USB\ SAMF/' + req.params[0]);
	});

	app.get('/', function(req, res) {
		res.render('index.ejs');
	});

	app.get('/prive_mdp', function(req, res) {
		res.render('prive.ejs');
	});

	app.get(/dl_(.+\.[a-z0-9]{3})$/i, function(req, res) {
		res.render('page_telechargement.ejs', {
			fichier : req.params[0] 
		});
	});

	app.get(/(.+vlc_.+\.[a-z0-9]{3})$/i, function(req, res) {
		res.render('lien_vlc.ejs');
	});

	app.get(/([a-z]{1,10}\/.+\.[a-z0-9]{3})$/i, function(req, res) {
		res.sendFile('/media/pi/USB\ SAMF/' + req.params[0]);
	});
	app.get(/([a-z]{1,10})\/?(.*)/, function(req, res) {
		fs.readdir('/media/pi/USB\ SAMF/' + req.params[0] + '/' + req.params[1], function(err, dossiers) {
			if(err) {
				res.end('cette ressource n\'est pas accessible');
				console.log(err);
			}
			res.render('gestion_fichiers.ejs', { 
				dossiers : dossiers
			});
		});
	});

	// erreur
	app.get(/(.*)/, function(req, res) {
		res.end('cette page n\'existe pas');
	});

}